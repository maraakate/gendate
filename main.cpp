/* This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org>
 */
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <process.h>

#pragma warning(disable : 4996)

/* Modify these constants to fit your needs */
static const char headerFile[] = "GIT_LAST_COMMIT.H";
static const char gitPipe[] = "git log -1 --pretty=\"%h (%ci)\"";
static const char gitError[] = "Unknown Commit.  Unknown Date.";
static const char headerDefine[] = "__GIT_LAST_COMMIT_H";
static const char gitDefine[] = "GIT_LAST_COMMIT";

static char gitBuffer[128];

static unsigned int getGitLastCommit (void)
{
	FILE *pipe;
	size_t pipeLen = 0;

	pipe = _popen(gitPipe, "rt");
	if(!pipe)
	{
		printf("Error: Couldn't open pipe for reading!\n");
		return 1;
	}

	fgets(gitBuffer, 128, pipe);
	pipeLen = strlen(gitBuffer);
	if (pipeLen)
		gitBuffer[pipeLen-1] = '\0';
	else
		strncpy(gitBuffer, gitError, sizeof(gitBuffer)-1);

	_pclose(pipe);

	return 0;
}

int main(int argc, char **argv)
{
	FILE *f = fopen(headerFile, "w");
	if (!f)
	{
		printf("Error: Couldn't open %s for writing!\n", headerFile);
		return 1;
	}

	fprintf(f, "#ifndef %s\n", headerDefine);
	fprintf(f, "#define %s\n", headerDefine);
	fprintf(f, "\n");
	if(!getGitLastCommit())
		fprintf(f, "#define %s \"%s\"\n", gitDefine, gitBuffer);
	else
		fprintf(f, "#define %s \"%s\"\n", gitDefine, gitError);
	fprintf(f, "\n");
	fprintf(f, "#endif // %s\n", headerDefine);

	fclose(f);

	return 0;
}

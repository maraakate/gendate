Gendate is a utility for generating a header file based on the
last git commit and date.

It's used by Daikatana v1.3 for using the #define in a printf
to the startup log for bug reports.

Simply modify main.cpp to fit your needs and import the project to your
Visual Studio project or compile with mingw.  Platforms with a bash shell
can use gengitdate.sh.

Updated project files, additional ports, powershell script version, etc.
are all appreciated.  Let me know if you make use of it or modify it!
